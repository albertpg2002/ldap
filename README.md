# LDAP - albertpg2002 M06-ASO
# Curs 2021-2022

* **albertpg2002/ldap21:base** container de treball base de LDAP. Organització: edt.org
* **albertpg2002/ldap21:editat** container de treball editat de LDAP. Organització: edt.org
* **albertpg2002/ldap21:schema** container de treball schema de LDAP. Organització: edt.org
* **albertpg2002/ldap21:practica** container de treball practic de LDAP. Organització: edt.org
* **albertpg2002/ldap21:acl** container de treball ACL's de LDAP. Organització: edt.org
