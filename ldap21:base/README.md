# LDAP Server
## Albert Pujadas ASIX M06-ASO 2021-2022
### Servidor LDAP (Debian 11)

Imatges docker al DockerHub de [edtasixm06](https://hub.docker.com/u/edtasixm06/)

Documentació del mòdul a [ASIX-M06](https://sites.google.com/site/asixm06edt/)

ASIX M06-ASO Escola del treball de barcelona


 * **Albertpg2002/ldap21:base** Servidor LDAP base inicial amb la base de dades edt.org

```
docker build -t Albertpg2002/ldap21:base .
docker run --rm --name ldap.edt.org -h ldap.edt.org -d Albertpg2002/ldap21:base
docker ps
ldapsearch -x -LLL -h ldap.edt.org -b 'dc=edt,dc=org'
``` 
