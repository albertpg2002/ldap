# LDAP Server
## ASIX M06-ASO 2021-2022
### Servidor LDAP (Debian 11)

Imatges docker al DockerHub de [edtasixm06](https://hub.docker.com/u/edtasixm06/)

Documentació del mòdul a [ASIX-M06](https://sites.google.com/site/asixm06edt/)

ASIX M06-ASO Escola del treball de barcelona


 * **albertpg02/ldap21:acl** Servidor LDAP editat amb la base de dades edt.org.
     * eliminats els schema innecesaris
definir la base de dades cn=config amb usuari
cn=sysadmin,cn=config i passw syskey
```
docker network create 2hisix
docker build -t albertpg2002/ldap21:editat .
docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisix -p 389:389 -d albertpg2002/ldap21:editat
docker ps
ldapsearch -x -LLL -h ldap.edt.org -b 'dc=edt,dc=org'
ldapsearch -x -LLL -D 'cn=Sysadmin,cn=config' -w syskey -b 'cn=config'
ldapsearch -x -LLL -D 'cn=Sysadmin,cn=config' -w syskey -b 'cn=config' olcDatabase={1}mdb
``` 
